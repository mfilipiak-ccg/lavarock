# LAVA ROCK

## Summary

This tool helps generate a application landscape diagram from yaml. Using a yaml defintion makes maintaining the diagram easier (in my opinion at least).

The motivation was to create a diagram very similar to https://landscape.cncf.io/ to illustrate the selected items from that chart. 

This tool can be used to create arbitrary landscape diagrams, as it is a useful way to layout data. If you need an exact diagram, just modify the `template.j2` CSS appropriately.

![Sample Diagram](samples/stack/stack.png)

## Usage

The input to this tool is a yaml file that defines the rows (called categories), sub-categories (called subs), and nodes (called pieces). You can optionally include images, although SVG images scale best.

### Minimal Example

```yaml
categories:
  - name: Waterski
    subs:
      - name: Skis
        pieces:
          - name: Connely
          - name: D3
          - name: Denali
          - name: Goode
          - name: HO
          - name: Radar
      - name: Boats
        pieces:
          - name: Nautique
            img: https://www.nautique.com/assets/img/svg/nautique-web-logo.svg
          - name: Malibu
          - name: MasterCraft
```

This is the example in `samples/minimal/data.yaml`. You may generate the diagram using

```sh
$ python3 lavarock.py samples/minimal/data.yaml > diagram.html
$ xdg-open diagram.html
```

Produces a responsive HTLM chart that looks like:

![Minimal Diagram](samples/minimal/minimal.png)