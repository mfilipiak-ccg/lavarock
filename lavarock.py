#!/usr/bin/python3
from typing import Dict
import sys
from yaml import safe_load

import requests
from jinja2 import FileSystemLoader, Environment
from bs4 import BeautifulSoup
from playwright.sync_api import sync_playwright



def inject_cncf_img_refs(input_yaml_dict: Dict[str, str], cncf_page: BeautifulSoup):
  for categories in input_yaml_dict['categories']:
    for sub_cats in categories["subs"]:
      for piece in sub_cats["pieces"]:
        img_ref = None
        if 'img-alt' in piece:
          piece["img"] = piece['img-alt']
        else:
          key = piece.get('img-alt-key', piece["name"])
          element = cncf_page.query_selector(f"img[alt=\"{key} logo\"]")
          if element:
              img_src = element.get_attribute('src').split('/')[-1]
              piece["img"] = f"https://landscape.cncf.io/logos/{img_src}"

def transform_template(filename, data):
  templateLoader = FileSystemLoader(searchpath='.')
  templateEnv = Environment(loader=templateLoader)
  template = templateEnv.get_template(filename)
  output = template.render(**data)
  print(output)

if __name__ == "__main__":
  if len(sys.argv) != 2:
    sys.exit("Usage: lavarock.py data.yaml")
    
  
  cncf_page = None
  with sync_playwright() as p:
    browser = p.chromium.launch()
    try:
      page = browser.new_page()
      page.goto("https://landscape.cncf.io/")

      # Wait for the dynamic content to load
      page.wait_for_selector('main')

      # Scrape data
      cncf_page = page #BeautifulSoup(page.content(), "html.parser")  # You can use page.evaluate(), page.query_selector(), etc., to get specific data

      data = None
      with open(sys.argv[1], 'r') as yamlfile:
        data = safe_load(yamlfile)
      inject_cncf_img_refs(data, cncf_page)
      transform_template("template.j2", data)
    finally:
      browser.close()
  
